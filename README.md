# update-deployment-tag
This is a container image that runs after a CI/CD build has deployed a new image.

It updates the image tag in the deployment YAML associated with the repo that was used to generate the image. The updated tag will be detected by ArgoCD, triggering a re-deployment with the latest image.

The arguments are:
1. project name
2. image tag
3. token to access the API
4. suffix of the related application deployment file
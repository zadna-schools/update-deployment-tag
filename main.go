package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/xanzy/go-gitlab"
)

func getDeploymentProjectID(projectName string, token string, applicationSuffix string) int {
	git, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	projects, _, err := git.Projects.ListProjects(&gitlab.ListProjectsOptions{
		Owned:  gitlab.Bool(true),
		Search: gitlab.String(projectName + "-" + applicationSuffix)})
	if err != nil {
		log.Fatalf("Failed to list Projects: %v", err)
	}

	projectID := 0
	if len(projects) == 1 {
		projectID = projects[0].ID
	} else {
		log.Fatalf("Failed to find deployment project for %v", projectName)
	}
	return projectID
}

func updateDeployment(projectAppPID int, projectName string, newTag string, token string, applicationSuffix string) {
	git, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	myFile, _, err := git.RepositoryFiles.GetFile(
		projectAppPID,
		"prod/deployment.yaml",
		&gitlab.GetFileOptions{Ref: gitlab.String("main")})
	if err != nil {
		log.Fatalf("Failed to find application deployment YAML: %v", err)
	}
	content, _ := base64.StdEncoding.DecodeString(myFile.Content)

	// Split deployment.yaml into pieces to isolate the image tag, then replace it and reassemble the content.
	parts := strings.Split(string(content[:]), "image: ")
	smallerParts := strings.Split(parts[1], "\n")
	smallerParts[0] = strings.Split(smallerParts[0], ":")[0] + ":" + newTag
	parts[1] = strings.Join(smallerParts, "\n")

	updateFileOptions := &gitlab.UpdateFileOptions{
		Branch:        gitlab.String("main"),
		Content:       gitlab.String(strings.Join(parts, "image: ")),
		CommitMessage: gitlab.String("Update image tag to " + newTag),
	}
	_, _, err = git.RepositoryFiles.UpdateFile(projectAppPID, "prod/deployment.yaml", updateFileOptions)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Updated deployment YAML with new tag", newTag, "in", projectName+"-app")
}

func main() {
	/*
	   This will update the image tag in the deployment YAML of the application repository of a project.
	   The arguments are:
	   1: project name
	   2: image tag
	   3: token to access the API
	   4: suffix of the related application deployment file
	*/
	updateDeployment(getDeploymentProjectID(os.Args[1], os.Args[3], os.Args[4]),
		os.Args[1],
		os.Args[2],
		os.Args[3],
		os.Args[4])
}
